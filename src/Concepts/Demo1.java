package Concepts;

import java.util.Scanner;

/**
 * 反射API
 */
public class Demo1 {

	public static void main(String[] args) {
		//一、動態加載類
		Scanner in = new Scanner(System.in);
		System.out.println("請輸入類名 : ");
		//動態獲得類名
		String className = in.nextLine();
		try {
			Class cls = Class.forName(className);
			System.out.println(cls);
			System.out.println(cls.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
