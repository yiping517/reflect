package Concepts;

import java.lang.reflect.Constructor;
import java.util.Scanner;

/**
 * 反射API
 */
public class Demo2 {

	public static void main(String[] args) {
		//一、動態創建對象
		Scanner in = new Scanner(System.in);
		System.out.println("請輸入類名 : ");
		String className = in.nextLine();
		try {
			Class cls = Class.forName(className);
			Constructor obj = cls.getConstructor();
			System.out.println(obj);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} 
		
	}

}
