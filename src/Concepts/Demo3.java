package Concepts;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * 反射API
 */
public class Demo3 {

	public static void main(String[] args) {
		//查詢類中的方法
		Scanner in = new Scanner(System.in);
		System.out.println("請輸入類名 : ");
		String className = in.nextLine();
		try {
			Class cls = Class.forName(className);
			
			Method[] methods = cls.getDeclaredMethods();
			for(Method meth:methods) {
				System.out.println(meth);
			}
			
			//動態創建對象
			Constructor con = cls.getDeclaredConstructor();
			Object obj = con.newInstance();
			
			//動態找到一個方法
			System.out.println("輸入方法名 : ");
			String name = in.nextLine();
			Method m = cls.getDeclaredMethod(name);
			//動態執行方法
			Object val = m.invoke(obj);
			//val是方法執行後的返回值
			
			System.out.println(val);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} 
		
	}

}
